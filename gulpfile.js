const
    // development mode
    dev = true,

    // modules
    gulp = require('gulp'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    htmlclean = require('gulp-htmlclean'),
    //htmlclean =  dev ? null : require('gulp-htmlclean'),  // moze i ova opcija razvoja
    sourcemaps = require('gulp-sourcemaps'),
    noop = require('gulp-noop'),
    terser = require('gulp-terser'),
    rename = require('gulp-rename'),
    concate = require('gulp-concat'),
    sync = require('browser-sync').create(),
    sass = require('gulp-sass'),

    // folders
    src = 'src/',
    public = 'public/';

// Images processing
function images() {
    const input = src + 'img/**/*';
    const output = public + 'slike/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(imagemin())
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// Video processing
function videos() {
    const input = src + 'video/**/*';
    const output = public + 'video/';

    return gulp.src(input)
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// HTML processing
function html() {
    const input = src + 'html/**/*';
    const output = public;

    return gulp.src(src + 'html/**/*')
        .pipe(newer(output))
        .pipe(dev ? noop() : htmlclean())
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// CSS processing
function css() {
    const input = src + 'scss/main.scss';
    const output = public + 'css';

    return gulp.src(input)
        .pipe(newer(output))
        //.pipe(dev ? sourcemaps.init() : noop())
        .pipe(sass({
            errLogToConsole: dev,
            outputStyle: dev ? 'expanded' : 'compressed'
        }).on('error', sass.logError))
        //.pipe(dev ? sourcemaps.write() : noop())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// JS processing
function js() {
    const input = [
        src + 'js/base/**/*',
        src + 'js/components/**/*',
        src + 'js/pages/**/*',
        src + 'js/main.js'
    ];
    const output = public + 'js';

    return gulp.src(input)
        .pipe(concate('app.js'))
        //.pipe(terser())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// php processing
function php() {
    const input = src + 'php/**/*';
    const output = public + 'php/';

    return gulp.src(input)
        .pipe(gulp.dest(output))
        .pipe(sync.stream());
}

// watch for file changes
function watch(done) {

    sync.init({
        server: {
            baseDir: './' + public
        }
    });

    sync.reload();

    // images changes
    gulp.watch(src + 'img/**/*', images);

    // video changes
    gulp.watch(src + 'video/**/*', videos);

    // html changes
    gulp.watch(src + 'html/**/*', html);

    // // scss changes
    gulp.watch(src + 'scss/**/*', css);

    // js changes
    gulp.watch(src + 'js/**/*', js);

    // php changes
    gulp.watch(src + 'php/**/*', php);

    done();
}

// Single tasks
// exports.html = html;
exports.html = gulp.series(images, videos, html);
exports.css = css;
exports.js = js;
exports.php = php;
exports.watch = watch;


// run all build tasks
exports.build = gulp.parallel(exports.html, exports.css, exports.js, exports.php);

// default task
exports.default = gulp.series(exports.build, exports.watch);