const handleMenuItemClick = () => {
    $('.navbar-nav li a').on('click', function (e) {
        if (!$(this).parent().hasClass('dropdown'))
            $('.navbar-collapse').collapse('hide');
    });
};

const handleBackToTop = () => {
    $('body').prepend(
        '<a href="#body" class="back-to-top page-scroll">Povratak na vrh</a>'
    );
    let amountScrolled = 500;
    $(window).scroll(function () {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('500');
        } else {
            $('a.back-to-top').fadeOut('500');
        }
    });
};

const handleInitWow = (options) => {
    if (options.initWow) {
        if (options.initWow.allow) {
            new WOW().init();
        }
    }
};

const handleVenobox = (options) => {
    if (options.venobox) {
        let venoboxClass = options.venobox.venoboxClass;
        $(venoboxClass).venobox();
    }
};

const hidePreloader = (options) => {
    if (options.preloader) {
        let element = options.preloader.element;
        let fadeOutTime = options.preloader.fadeOutTime;

        let loader = $(element);
        setTimeout(() => {
            loader.fadeOut(fadeOutTime);
        }, 500);
    }
};

const toBoolean = (str) => {
    if (typeof str === 'undefined' || str === null) {
        return false;
    } else if (typeof str === 'string') {
        switch (str.toLowerCase()) {
            case 'false':
            case 'no':
            case '0':
            case '':
                return false;
            default:
                return true;
        }
    } else if (typeof str === 'number') {
        return str !== 0;
    } else {
        return true;
    }
};
