/*  Application Controller
---------------------------------------------------- */

const App = (() => {
    'use strict';

    return {
        init: function (options) {
            this.initBase(options);
            this.initComponents(options);
            this.initPages(options);
        },
        initBase: (options) => {
            handleInitWow(options);
            handleVenobox(options);
            hidePreloader(options);
        },
        initComponents: (options) => {
            handleCounterScroll(options);
        },
        initPages: (options) => {
            handleBackToTop();
            handleMenuItemClick();

            handleCallMe(options);

            handlePrivacy(options);
            handleContact(options);
            handleProjects(options);
            handleProjectsVideo(options);
            handleSliders(options);
            handleLocation(options);
        }
    };
})();
