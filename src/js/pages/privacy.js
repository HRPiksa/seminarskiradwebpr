const handlePrivacy = (options) => {
    if (options.privacy) {
        let form = options.privacy.form;
        let msgSubmit = options.privacy.msgSubmit;

        $(form)
            .validator()
            .on('submit', function (event) {
                if (event.isDefaultPrevented()) {
                    pformError();
                    psubmitMSG(false, 'Molimo popunite sva polja!');
                } else {
                    event.preventDefault();
                    psubmitForm();
                }
            });

        const psubmitForm = () => {
            let data = $(form).serializeArray();
            let dataObj = {};

            $.each(data, (index, value) => {
                dataObj[value.name] = value.value === '' ? null : value.value;
            });

            $.ajax({
                url: 'http://myremotephp.sytes.net:22080/seminar/privacy.php',
                type: 'POST',
                data: dataObj,
                success: function (text) {
                    if (text == 'Privacy Request Sent Successfully') {
                        pformSuccess();
                    } else {
                        pformError();
                        psubmitMSG(false, text);
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
            });
        }

        function pformSuccess() {
            $(form)[0].reset();
            psubmitMSG(true, 'Poslan zahtjev za privatnost!');
        }

        function pformError() {
            $(form)
                .removeClass()
                .addClass('shake animated')
                .one('animationend', function () {
                    $(this).removeClass();
                });
        }

        function psubmitMSG(valid, msg) {
            if (valid) {
                var msgClasses = 'h3 text-center tada animated';
            } else {
                var msgClasses = 'h3 text-center';
            }
            $(msgSubmit).removeClass().addClass(msgClasses).text(msg);
        }
    }
};
