const handleProjects = function (options) {
    if (options.projects) {
        let popup = options.projects.popup;
        let popupMainClass = options.projects.popupMainClass;
        let popupItemSelector = options.projects.popupItemSelector;
        let grid = options.projects.grid;
        let filtersBtnGroup = options.projects.filtersBtnGroup;
        let popupDataFilter = options.projects.popupDataFilter;
        let btnGroup = options.projects.btnGroup;

        $(popup).magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: popupMainClass,
        });

        var $grid = $(grid).isotope({
            itemSelector: popupItemSelector,
            layoutMode: 'fitRows',
        });

        $(filtersBtnGroup).on('click', 'a', function () {
            var filterValue = $(this).attr(popupDataFilter);
            $grid.isotope({
                filter: filterValue,
            });
        });

        $(btnGroup).each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'a', function () {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });
    }
};

const handleProjectsVideo = (options) => {
    if (options.projectsVideo) {
        let videoPlayer = document.querySelector(
            options.projectsVideo.playerId
        );
        let fileType = options.projectsVideo.fileType;
        let links = videoPlayer.getElementsByTagName('a');
        for (var i = 0; i < links.length; i++) {
            links[i].onclick = function (e) {
                e.preventDefault();
                videoTarget = this.getAttribute('href');
                fileName =
                    videoTarget.substr(0, videoTarget.lastIndexOf('.')) ||
                    videoTarget;
                video = document.querySelector('#video-player video');
                video.removeAttribute('poster');
                source = document.querySelectorAll(
                    '#video-player video source'
                );
                source[0].src = fileName + '.' + fileType;
                video.load();
                video.play();
            };
        }
    }
};
