const handleLocation = (options) => {
    if (options.location) {
        let lat = options.location.lat;
        let lng = options.location.lng;

        var directionsService;
        var directionsDisplay;
        var geocoder;
        var map;
        var infoWindow;

        document
            .getElementById('directions')
            .addEventListener('click', onChangeHandler);

        function onChangeHandler(event) {
            event.preventDefault();
            event.stopPropagation();

            if (infoWindow) {
                infoWindow.close();
            }

            if (document.getElementById('startLocation').value == '') {
                foundYou(null);
            } else {
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            }
        }

        initMap = (lat, lng) => {
            directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer({
                polylineOptions: {
                    strokeColor: '#66ff66',
                    strokeWeight: 7,
                    strokeOpacity: 1,
                },
            });
            geocoder = new google.maps.Geocoder();
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 15,
                center: {
                    lat: lat,
                    lng: lng,
                },
            });
            directionsDisplay.setMap(map);

            map.addListener('click', function (mapsMouseEvent) {
                searchLocation(mapsMouseEvent.latLng);
                if (infoWindow) {
                    infoWindow.close();
                }

                infoWindow = new google.maps.InfoWindow({
                    position: mapsMouseEvent.latLng,
                });
                infoWindow.setContent(mapsMouseEvent.latLng.toString());
                infoWindow.open(map);
            });

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(foundYou);
            } else {
                alert('Geolokacija nije podržana ili nije omogućena.');
            }
        };

        foundYou = (position) => {
            const latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode(
                {
                    latLng: latlng,
                },
                (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                            });
                            let address =
                                results[0].address_components[1].long_name +
                                ' ' +
                                results[0].address_components[0].long_name +
                                ', ' +
                                results[0].address_components[2].long_name +
                                ', ' +
                                results[0].address_components[3].long_name;

                            document.getElementById(
                                'startLocation'
                            ).value = address;
                        }
                    } else {
                        alert('Geocoder nije uspio zbog: ' + status);
                    }
                }
            );
        };

        searchLocation = (position) => {
            geocoder.geocode(
                {
                    latLng: position,
                },
                (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            let address =
                                results[0].address_components[1].long_name +
                                ' ' +
                                results[0].address_components[0].long_name +
                                ', ' +
                                results[0].address_components[2].long_name +
                                ', ' +
                                results[0].address_components[3].long_name;

                            if (
                                document.getElementById('startLocation')
                                    .value == ''
                            ) {
                                document.getElementById(
                                    'startLocation'
                                ).value = address;
                            } else {
                                document.getElementById(
                                    'endLocation'
                                ).value = address;
                            }
                        }
                    } else {
                        alert('Geocoder nije uspio zbog: ' + status);
                    }
                }
            );
        };

        calculateAndDisplayRoute = (directionsService, directionsDisplay) => {
            directionsService.route(
                {
                    origin: document.getElementById('startLocation').value,
                    destination: document.getElementById('endLocation').value,
                    travelMode: google.maps.TravelMode.DRIVING,
                },
                (response, status) => {
                    if (status === google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    } else {
                        window.alert(
                            'Zahtjev za rutu nije uspio zbog ' + status
                        );
                    }
                }
            );
        };

        google.maps.event.addDomListener(window, 'load', initMap(lat, lng));
    }
};
