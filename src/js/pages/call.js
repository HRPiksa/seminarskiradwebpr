let handleCallMe = (options) => {
    if (options.callme) {
        let form = options.callme.form;
        let msgSubmit = options.callme.msgSubmit;

        $(form)
            .validator()
            .on('submit', (event) => {
                if (event.isDefaultPrevented()) {
                    lformError();
                    lsubmitMSG(false, 'Molimo popunite sva polja!');
                } else {
                    event.preventDefault();
                    lsubmitForm();
                }
            });

        const lsubmitForm = () => {
            let data = $(form).serializeArray();
            let dataObj = {};

            $.each(data, (index, value) => {
                dataObj[value.name] = value.value === '' ? null : value.value;
            });

            $.ajax({
                url: 'http://myremotephp.sytes.net:22080/seminar/callme.php',
                type: 'POST',
                data: dataObj,
                success: function (text) {
                    if (text == 'Request Sent Successfully') {
                        lformSuccess();
                    } else {
                        lformError();
                        lsubmitMSG(false, text);
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
            });
        }

        function lformSuccess() {
            $(form)[0].reset();
            $(form).parsley().reset();
            lsubmitMSG(true, 'Zahtjev je poslan!');
        }

        function lformError() {
            $(form)
                .removeClass()
                .addClass('shake animated')
                .one('animationend', function () {
                    $(this).removeClass();
                });
        }

        function lsubmitMSG(valid, msg) {
            if (valid) {
                var msgClasses = 'h3 text-center tada animated';
            } else {
                var msgClasses = 'h3 text-center';
            }
            $(msgSubmit).removeClass().addClass(msgClasses).text(msg);
        }
    }
};
