const handleContact = (options) => {
    if (options.contact) {
        let form = options.contact.form;
        let msgSubmit = options.contact.msgSubmit;
        let dateInput = options.contact.dateInput;

        setInputDate(dateInput);

        $(form)
            .validator()
            .on('submit', (event) => {
                if (event.isDefaultPrevented()) {
                    cformError();
                    csubmitMSG(false, 'Molimo popunite sva polja!');
                } else {
                    event.preventDefault();
                    csubmitForm();
                }
            });

        const csubmitForm = () => {
            let data = $(form).serializeArray();
            let dataObj = {};

            $.each(data, (index, value) => {
                dataObj[value.name] = value.value === '' ? null : value.value;
            });

            $.ajax({
                url: 'http://myremotephp.sytes.net:22080/seminar/contact.php',
                type: 'POST',
                data: dataObj,
                success: function (text) {
                    if (text == 'Mail Sent Successfully') {
                        cformSuccess();
                    } else {
                        cformError();
                        csubmitMSG(false, text);
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                },
            });
        };

        function cformSuccess() {
            $(form)[0].reset();
            csubmitMSG(true, 'Poruka poslana!');
        }

        function cformError() {
            $(form)
                .removeClass()
                .addClass('shake animated')
                .one('animationend', function () {
                    $(this).removeClass();
                });
        }

        function csubmitMSG(valid, msg) {
            if (valid) {
                var msgClasses = 'h3 text-center tada animated';
            } else {
                var msgClasses = 'h3 text-center';
            }
            $(msgSubmit).removeClass().addClass(msgClasses).text(msg);
        }
    }
};

const setInputDate = (idDate) => {
    let dat = document.querySelector(idDate);
    dat.value = moment().format('YYYY-MM-DD');
};
