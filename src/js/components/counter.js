const handleCounterScroll = (options) => {
    if (options.counter) {
        let counterId = options.counter.counterId;
        let counterValue = options.counter.counterValue;
        let dataCount = options.counter.dataCount;

        let a = 0;
        $(window).scroll(() => {
            if ($(counterId).length) {
                let oTop = $(counterId).offset().top - window.innerHeight;
                if (a == 0 && $(window).scrollTop() > oTop) {
                    $(counterValue).each(function () {
                        let $this = $(this),
                            countTo = $this.attr(dataCount);
                        $({
                            countNum: $this.text()
                        }).animate({
                            countNum: countTo
                        }, {
                            duration: 2000,
                            easing: 'swing',
                            step: function () {
                                $this.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                $this.text(this.countNum);
                            }
                        });
                    });
                    a = 1;
                }
            }
        });
    }
}