const handleSliders = (options) => {
    if (options.sliders) {
        let swiperEl = options.sliders.swiperEl;
        let nextEl = options.sliders.nextEl;
        let prevEl = options.sliders.prevEl;
        let delay = options.sliders.delay;

        var cardSlider = new Swiper(swiperEl, {
            autoplay: {
                delay: delay,
                disableOnInteraction: false
            },
            loop: true,
            navigation: {
                nextEl: nextEl,
                prevEl: prevEl
            },
            slidesPerView: 3,
            spaceBetween: 20,
            breakpoints: {
                992: {
                    slidesPerView: 2
                },
                768: {
                    slidesPerView: 1
                }
            }
        });
    }
}